const express = require('express');
const router = express.Router();

const logger = require('../log/logger').Logger;
const authentication = require('../config/authentication_api');

// Authenticate user
router.get('/authenticate', (req, res, next) => {

    let user = {
        username:req.query.username,
        password:req.query.password
    };

    authentication.authenticateUser(user, (err, response, body) => {
        try {
            var token = JSON.parse(body);
            res.json(token);
        } 
        catch(e){
            console.log("Authentication error!");
            logger.error("Authentication error");
            res.json({
                success:false
            });
        }
    });

});

module.exports = router;