const express = require('express');
const router = express.Router();

const Event = require('../models/events');
var tabify = require('es-tabify');
const logger = require('../log/logger').Logger;

// Get first event of a line
router.get('/get_first_event', (req, res, next) => {

    let newdata = {
        line:req.query.line,
        date:req.query.date,
        factory:req.query.factory,
        hsk:req.query.hsk,
        at:req.query.at
    };

    Event.getFirstEvent(newdata, (error, response) => {
        
        if (error){
            console.log("search error: "+error);
            logger.error(error);
        }
        else {
            res.json(response);
        }

    });

});

// Get Time Event
router.get('/get_time_event', (req, res, next) => {

    let newdata = {
        index:req.query.index,
        line:req.query.line
    };

    Event.getTimeEvent( newdata, (error, response) => {
        
        if (error){
            console.log("search error: "+error);
            logger.error(error);
        }
        else {
            res.json(response);
        }

    });

});

router.get('/get_all_indexes', (req, res, next) => {

    Event.getAllTimeOpsIndexes( (error, response) => {
        
        if (error){
            console.log("search error: "+error);
            logger.error(error);
        }
        else {
            res.json(response);
        }

    });

});

router.get('/get_all_subjects_for_index', (req, res, next) => {

    let newdata = {
        index:req.query.index
    };

    Event.getAllSubjectsForIndex( newdata, (error, response) => {
        
        if (error){
            console.log("search error: "+error);
            logger.error(error);
        }
        else {
            res.json(response);
        }

    });

});

router.get('/get_all_first_events', (req, res, next) => {

    let newdata = {
        date:req.query.date,
        factory:req.query.factory
    };

    Event.getAllFirstEvents( newdata, (error, response) => {
        
        if (error){
            console.log("search error: "+error);
            logger.error(error);
        }
        else {
            var data = tabify(response);
            res.json(data);
        }

    });

});

router.get('/get_all_member_events', (req, res, next) => {

    let newdata = {
        date:req.query.date,
        factory:req.query.factory
    };

    Event.getAllMemberEvents( newdata, (error, response) => {
        
        if (error){
            console.log("search error: "+error);
            logger.error(error);
        }
        else {
            var data = tabify(response);
            res.json(data);
        }

    });

});

// Test routes
router.get('/test_route', (req, res, next) => {

    res.json({
        success:true, 
        msg:'Routes are working!'
    });

});


module.exports = router;

