var client_elog = require('../config/es_elog_connection');
var client_ops = require('../config/es_ops_connection');

module.exports.getFirstEvent = function(data, callback){

    var index = '*-' + data.factory + '-activity-elog-' + data.date;
    var line = '*-' + data.factory + '-' + data.line;
    var hsk = data.hsk;
    var at = data.at;
    
    client_elog.search({

        index: index,
        body: {
            "size": 1,
            "sort": [
                {
                    "w1.dt": {
                        "order": "asc"
                    }
                }
            ], 
            "query": {
                "bool": {
                    "must": [
                        {
                        "wildcard":
                            { 
                                "a.sub.keyword": line
                            }
                        },
                        {
                            "term": 
                            {
                                "a.aux.hsk.keyword": hsk
                            }
                        },
                        {
                            "term":
                            {
                                "w2.at.keyword": at
                            }
                        }
                    ]
                }
            },
            "_source": ["w1.dt","w1.os"]
        }
        
    }, callback);

};

module.exports.getTimeEvent = function(data, callback){

    var index = data.index;
    var subject = data.line;
    
    client_ops.search({

        index: "*ops-time*",
        body: {

            "size":0,
            "_source":
            {
                "excludes":[]
            },
            "aggs":
            {
                "2":
                {
                    "date_histogram":
                    {
                        "field":"time_tags.datetime",
                        "interval":"1m",
                        "time_zone":"Asia/Kolkata",
                        "min_doc_count":1
                    },
                    "aggs":
                    {
                        "1":
                        {
                            "sum":
                            {
                                "field":"points.attrib_val"
                            }
                        }
                    }
                }
            },
            "stored_fields":["*"],
            "script_fields":{},
            "docvalue_fields":
            [
                "time_tags.datetime",
                "time_tags.sessionDate"
            ],
            "query":
            {
                "bool":
                {
                    "must":
                    [
                        {
                            "match_all":{}
                        },
                        {
                            "match_phrase":
                            {
                                "_index":
                                {
                                    "query":index
                                }
                            }
                        },
                        {
                            "term": 
                            {
                                "subject":subject
                            }
                        }
                    ]
                }
            }
        }
        
    }, callback);

};

module.exports.getAllTimeOpsIndexes = function(callback){
    
    client_ops.search({

        index: "*ops-time*",
        body: {
            
            "size": 0,
            "query": 
            {
                "bool": 
                {
                    "filter": []
                }
            },
            "aggs": 
            {
                "1": 
                {
                    "terms": 
                    {
                        "field": "_index",
                        "size": 1000
                    }
                }
            }
        }
        
    }, callback);

};

module.exports.getAllSubjectsForIndex = function(data,callback){

    var index = data.index;
    
    client_ops.search({

        index: index,
        body: {
            
            "size": 0,
            "query": 
            {
                "bool": 
                {
                    "filter": []
                }
            },
            "aggs": 
            {
                "1": 
                {
                    "terms": 
                    {
                        "field": "subject",
                        "size": 1000
                    }
                }
            }
        }
        
    }, callback);

};

module.exports.getAllFirstEvents = function(data, callback){

    var index = '*-' + data.factory + '-activity-elog-' + data.date;
    
    client_elog.search({

        index: index,
        body: {
            
            "size": 0,
            "sort": [
                {
                    "w1.dt": {
                        "order": "asc"
                    }
                }
            ],
            "query": {
                "bool": {
                "filter" : []
                }
            },
            "aggs": {
                "at": {
                    "terms": {
                        "field": "w2.at.keyword",
                        "size": 1000
                    },
                    "aggs": {
                        "sub": {
                            "terms": {
                                "field": "a.sub.keyword",
                                "size": 1000
                            },
                            "aggs": {
                                "hsk": {
                                    "terms": {
                                        "field": "a.aux.hsk.keyword",
                                        "size": 1000
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }, callback);

};

module.exports.getAllMemberEvents = function(data, callback){

    var index = '*-' + data.factory + '-ops-cate-*';
    
    client_ops.search({

        index: index,
        body: {
            
            "size": 1,
            "query": {
                "bool": {
                    "filter": [
                        {
                            "regexp": {
                                "time_corpus": {
                                    "value": data.date + "-shift1-<1-24>"
                                }
                            }
                        },
                        {
                            "terms": {
                                "attribute": [
                                    "member"
                                ]
                            }
                        }
                    ]
                }
            },
            "aggs": {
                "factory": {
                    "terms": {
                        "field": "org_tags.factory_id",
                        "size": 10,
                        "order": {
                            "_key": "asc"
                        }
                    },
                    "aggs": {
                        "dpt": {
                            "terms": {
                                "field": "org_tags.dpt_id",
                                "size": 10,
                                "order": {
                                    "_key": "asc"
                                }
                            },
                            "aggs": {
                                "module": {
                                    "terms": {
                                        "field": "org_tags.module_id",
                                        "size": 100,
                                        "order": {
                                            "_key": "asc"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }, callback);

};



