var querystring = require('querystring');
var request = require('request');

module.exports.authenticateUser = function(data, callback){

    var formData = querystring.stringify(data);
    var contentLength = formData.length;

    request({
        headers: {
          'Content-Length': contentLength,
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        uri: 'https://nfactorylive.ncinga.com/app_auth_services/login?client_name=DirectFormClient',
        body: formData,
        method: 'POST'
    }, callback);
    
};