const express = require('express');
const cors = require('cors');
const logger = require('./log/logger').Logger;

const app = express();

const events = require('./routes/events');
const users = require('./routes/users');

// Port number
const port = 3000;

// CORS Middleware
app.use(cors());

app.use('/events',events);
app.use('/users',users);

// Index Route
app.get('/', (req,res) => {
    res.send('Invalid Endpoint');
});

// Start Server
app.listen(port, () => {
    console.log('Server started on port '+ port);
    logger.info('Server started on port ' + port);
});