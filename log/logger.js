var fs = require('fs');
var Logger = exports.Logger = {};

var infoStream = fs.createWriteStream('log/info.log',{flags: 'a'});
var errorStream = fs.createWriteStream('log/error.log',{flags: 'a'});
var debugStream = fs.createWriteStream('log/debug.log',{flags: 'a'});

var tzoffset = (new Date()).getTimezoneOffset() * 60000;
var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1);


Logger.info = function(msg) {
    var message = localISOTime + " : " + msg + "\n";
    infoStream.write(message);
};

Logger.debug = function(msg) {
    var message = localISOTime + " : " + msg + "\n";
    debugStream.write(message);
};

Logger.error = function(msg) {
    var message = localISOTime + " : " + msg + "\n";
    errorStream.write(message);
};